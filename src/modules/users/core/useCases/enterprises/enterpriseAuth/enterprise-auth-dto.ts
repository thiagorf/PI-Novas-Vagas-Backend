export interface EnterpriseAuthDTO {
    email: string;
    password: string;
}
