export interface Enterprise {
    id: number;
    cnpj: string;
    segment: string;
    cep: number;
    user_id: number;
}
